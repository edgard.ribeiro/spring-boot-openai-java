package br.com.alura.ecomart.chatbot.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.alura.ecomart.chatbot.infra.openai.DadosRequisicaoChatCompletion;
import br.com.alura.ecomart.chatbot.infra.openai.OpenAIClient;

@Service
public class ChatBotService {
	
	private OpenAIClient client;	
		
	public ChatBotService(OpenAIClient client) {		
		this.client = client;
	}

	public String responderPergunta(String pergunta) {
		var promptSistema = "Você é um chatbot de atendimento a clientes de um ecommerce e deve responder apenas perguntas relacionadas com ecommerce";
		var dados = new DadosRequisicaoChatCompletion(promptSistema, pergunta);
		return client.enviarRequisicaoChatCompletion(dados);		
	}
	
	public List<String> carregarHistorico(){
		return client.carregarHistoricoDeMensagens();
	}

	public void limparHistorico() {
		client.apagarThread();
		
	}

}
