package br.com.alura.ecomart.chatbot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.alura.ecomart.chatbot.domain.service.ChatBotService;
import br.com.alura.ecomart.chatbot.web.dto.PerguntaDto;

@Controller
@RequestMapping({"/", "chat"})
public class ChatController {

    private static final String PAGINA_CHAT = "chat";
    
    private ChatBotService chatBotService;    

    public ChatController(ChatBotService chatBotService) {
		this.chatBotService = chatBotService;
	}

	@GetMapping
    public String carregarPaginaChatbot(Model model) {
		var mensagens = chatBotService.carregarHistorico();
		model.addAttribute("historico", mensagens);
        return PAGINA_CHAT;
    }

    @PostMapping
    @ResponseBody
    public String responderPergunta(@RequestBody PerguntaDto dto) {
    	return chatBotService.responderPergunta(dto.pergunta());
    }

    @GetMapping("limpar")
    public String limparConversa() {
    	chatBotService.limparHistorico();    	
        return "redirect:/chat";
    }

}
